package fr.efficom.gesetudiant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GesetudiantApplication {

	public static void main(String[] args) {
		SpringApplication.run(GesetudiantApplication.class, args);
	}

}
