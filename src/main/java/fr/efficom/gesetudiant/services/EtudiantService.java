package fr.efficom.gesetudiant.services;

import fr.efficom.gesetudiant.domain.Etudiant;
import fr.efficom.gesetudiant.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EtudiantService {

    @Autowired
    private EtudiantRepository etudiantRepository;

    public Etudiant createEtudiant(Etudiant et){
        return etudiantRepository.save(et);
    }

    public List<Etudiant> findAllEtudiants() {
        return etudiantRepository.findAll();
    }

    public Etudiant updateEtudiant(int id, Etudiant et) {
        Optional<Etudiant> e = etudiantRepository.findById(id);
        if(e.isPresent()) {
            Etudiant etudiant = e.get();
            etudiant.setNom(et.getNom());
            etudiant.setPrenom(et.getPrenom());
            return etudiantRepository.save(etudiant);
        }
        return new Etudiant();
    }

    public void deleteEtudiant(int id) {
        Optional<Etudiant> e = etudiantRepository.findById(id);
        if(e.isPresent()) {
            Etudiant etudiant = e.get();
            etudiantRepository.delete(etudiant);
        }
    }
}
