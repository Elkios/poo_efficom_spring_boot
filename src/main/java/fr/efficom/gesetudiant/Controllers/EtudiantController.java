package fr.efficom.gesetudiant.Controllers;

import fr.efficom.gesetudiant.domain.Etudiant;
import fr.efficom.gesetudiant.services.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EtudiantController {

    @Autowired
    private EtudiantService etudiantService;

    @PostMapping("api/etudiants")
    public ResponseEntity<Etudiant> createEtudiant(@RequestBody Etudiant et) {
        return new ResponseEntity<>(etudiantService.createEtudiant(et), HttpStatus.CREATED);
    }

    @GetMapping("api/etudiants")
    public ResponseEntity<List<Etudiant>> findAllEtudiants() {
        return new ResponseEntity<>(etudiantService.findAllEtudiants(), HttpStatus.OK);
    }

    @PutMapping("api/etudiants/{id}")
    public ResponseEntity<Etudiant> updateEtudiant(@PathVariable("id") int id, @RequestBody Etudiant et) {
        return new ResponseEntity<>(etudiantService.updateEtudiant(id, et), HttpStatus.OK);
    }

    @DeleteMapping("api/etudiants/{id}")
    public ResponseEntity<Void> deleteEtudiant(@PathVariable("id") int id) {
        etudiantService.deleteEtudiant(id);
        return ResponseEntity.noContent().build();
    }

}
