package fr.efficom.gesetudiant.repository;

import fr.efficom.gesetudiant.domain.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EtudiantRepository extends JpaRepository<Etudiant, Integer> {

    Optional<Etudiant> findById(int id);
    List<Etudiant> findAll();
    void deleteById(int id);
    @Query(value = "SELECT * from etudiant e where e.nom = :name", nativeQuery = true)
    List<Etudiant> findEtudiantByName(@Param("name") String name);

}
